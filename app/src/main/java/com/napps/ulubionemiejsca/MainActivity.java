package com.napps.ulubionemiejsca;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import java.util.ArrayList;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;


public class MainActivity extends AppCompatActivity implements SaveLocation.OnNewDataAvailable, RecyclerItemClickListener.OnRecyclerClickListener, NumberPicker.OnValueChangeListener {
    private RecyclerViewAdapter mRecyclerViewAdapter;
    FloatingActionButton fab = null;
    private BaseManagement bm;
    SaveLocation saveLocation;
    private static final int REQUEST_CODE = 34;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView =  findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        int hasReadLocationPermission = ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION);
        Log.d("ta", "onCreate: checkSelfPermission = " + hasReadLocationPermission);
        if(hasReadLocationPermission != PackageManager.PERMISSION_GRANTED) {
            Log.d("ta", "onCreate: requesting permissions");
            ActivityCompat.requestPermissions(this, new String[] {ACCESS_COARSE_LOCATION}, REQUEST_CODE);
        }

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, this));

        mRecyclerViewAdapter = new RecyclerViewAdapter(this, new ArrayList<Locations>());
        recyclerView.setAdapter(mRecyclerViewAdapter);
        bm = new BaseManagement(this);
        mRecyclerViewAdapter.loadLocations(bm.loadData());

        fab = findViewById(R.id.addLocationButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = MainActivity.this;
                MainActivity activityCompat = MainActivity.this;
                saveLocation = new SaveLocation(MainActivity.this, view, context, activityCompat);
                saveLocation.makeSave();
            }
        });
    }
    @Override
    public void onItemClick(View view, final int position) {
        final Dialog d = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        d.setTitle(((bm.loadData()).get(position)).getDescription());
        d.setContentView(R.layout.dialog2);
        Button changePositionButton = d.findViewById(R.id.changePositionButton);
        Button cancelButton = d.findViewById(R.id.cancelButton);
        Button deleteButton = d.findViewById(R.id.deleteButton);
        changePositionButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
            changePosition(((bm.loadData()).get(position)).getId());
                d.dismiss();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String description = ((bm.loadData()).get(position)).getDescription();
                bm.updateID(bm.countItems(),((bm.loadData()).get(position)).getId());
                bm.removeSlot(description);
                mRecyclerViewAdapter.loadLocations(bm.loadData());
                d.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }
    @Override
    public void onNewDataAvailable() {
        mRecyclerViewAdapter.loadLocations(bm.loadData());

    }
    void changePosition(final int position){
        final Dialog d = new Dialog(MainActivity.this, R.style.ThemeOverlay_AppCompat_Dialog_Alert );
        d.setTitle("Number of repetitions:");
        d.setContentView(R.layout.dialog3);
        Button setButton =  d.findViewById(R.id.setButton);
        Button cancelButton =  d.findViewById(R.id.cancelButton);
        final NumberPicker np = d.findViewById(R.id.newPosition);
        np.setMaxValue(bm.countItems());
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        setButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                bm.updateID(np.getValue(), position);
                mRecyclerViewAdapter.loadLocations(bm.loadData());
                d.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }
    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {

    }
}