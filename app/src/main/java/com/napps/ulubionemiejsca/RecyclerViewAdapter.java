package com.napps.ulubionemiejsca;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Rewan on 2017-06-08.
 */
//I took copy of that class from my other project and modified for this project

class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.LocationViewHolder> {
    private static final String TAG = "RecyclerViewAdapt";
    private List<Locations> mList;
    private Context mContext;

    public RecyclerViewAdapter(Context context, List<Locations> dataList) {
        mContext = context;
        mList = dataList;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: new view requested");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browse, parent, false);
        return new LocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {

        if ((mList == null) || (mList.size() == 0)) {

        } else {
            Locations locationsItem = mList.get(position);
            Log.d(TAG, "onBindViewHolder: " + locationsItem.getDescription() + " --> " + position);

            holder.description.setText(locationsItem.getDescription());
            holder.firstCoordinate.setText(String.valueOf(locationsItem.getFirstCoordinate()));
            holder.secondCoordinate.setText(String.valueOf(locationsItem.getSecondCoordinate()));

        }
    }

    @Override
    public int getItemCount() {
        return ((mList != null) && (mList.size() != 0) ? mList.size() : 1);
    }

    void loadLocations(List<Locations> locationsList) {
        mList = locationsList;
        notifyDataSetChanged();
    }


    static class LocationViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ViewHolder";
        TextView firstCoordinate = null;
        TextView secondCoordinate = null;
        TextView description = null;

        public LocationViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "ViewHolder: starts");
            this.firstCoordinate = (TextView) itemView.findViewById(R.id.firstCoordinate);
            this.secondCoordinate = (TextView) itemView.findViewById(R.id.secondCoordinate);
            this.description = (TextView) itemView.findViewById(R.id.description);
        }
    }
}