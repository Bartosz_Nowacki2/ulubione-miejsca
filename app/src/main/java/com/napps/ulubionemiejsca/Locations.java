package com.napps.ulubionemiejsca;


class Locations {
    private int m_Id;
    private String mDescription;
    private double mFirstCoordinate;
    private double mSecondCoordinate;
    private int mOrderNumber;

Locations(String description, double firstCoordinate, double secondCoordinate){
    this.mDescription = description;
    this.mFirstCoordinate = firstCoordinate;
    this.mSecondCoordinate = secondCoordinate;
}
     Locations() {
        mDescription = "";
        mFirstCoordinate = 0;
        mSecondCoordinate = 0;
    }

     int getId() {
        return m_Id;
    }
     void setId(int id) {
        this.m_Id = id;
    }

     String getDescription() {
        return mDescription;
    }
    void setDescription(String description) {
        this.mDescription = description;
    }

     double getFirstCoordinate() {
        return mFirstCoordinate;
    }
     void setFirstCoordinate(double firstCoordinate) {
        this.mFirstCoordinate = firstCoordinate;
    }

    double getSecondCoordinate() {
        return mSecondCoordinate;
    }
    void setSecondCoordinate(double secondCoordinate) {
        this.mSecondCoordinate = secondCoordinate;
    }

    @Override
    public String toString() {
        return "Locations{" +
                "id=" + m_Id +
                ", mDescription='" + mDescription + '\'' +
                ", mFirstCoordinate=" + mFirstCoordinate +
                ", mSecondCoordinate=" + mSecondCoordinate +
                '}';
    }
}
