package com.napps.ulubionemiejsca;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

//I took copy of that class from my other project and modified for this project

class BaseManagement extends SQLiteOpenHelper{

    BaseManagement(Context context) {
        super(context, "saveddata.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(
                "CREATE TABLE locations(" +
                        "id INTEGER PRIMARY KEY NOT NULL," +
                        "description TEXT," +
                        "firstCoordinate DOUBLE," +
                        "secondCoordinate DOUBLE);" +
                        "");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    void addSlot(Locations locations){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("firstCoordinate", locations.getFirstCoordinate());
        values.put("secondCoordinate", locations.getSecondCoordinate());
        values.put("description", locations.getDescription());
        db.insertOrThrow("locations",null, values);
    }
    private void addSlot(Locations locations, int id){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("firstCoordinate", locations.getFirstCoordinate());
        values.put("secondCoordinate", locations.getSecondCoordinate());
        values.put("description", locations.getDescription());
        values.put("id", id);
        db.insertOrThrow("locations",null, values);
    }

    void removeSlot(String description){
        SQLiteDatabase db = getWritableDatabase();
        String[] arguments={description};
        db.delete("locations", "description=?", arguments);
    }
    List<Locations> loadData(){
        List<Locations> mList = new ArrayList<>();
        String[] columns={"id", "description", "firstCoordinate", "secondCoordinate"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor =db.query("locations",columns,null,null,null,null,null);
        for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
            Locations locations = new Locations();
            locations.setId(cursor.getInt(0));
            locations.setDescription(cursor.getString(1));
            locations.setFirstCoordinate(cursor.getDouble(2));
            locations.setSecondCoordinate(cursor.getDouble(3));
            mList.add(locations);
        }
        cursor.close();
        return mList;
    }
    int countItems(){
        SQLiteDatabase db = getWritableDatabase();
        return db.rawQuery("SELECT id FROM locations", null).getCount();

    }
    void updateID(int newID, int oldID){
        SQLiteDatabase db = getWritableDatabase();
        List<Locations> mList  = loadByID(oldID);
        Locations locations = mList.get(0);
        removeSlot(locations.getDescription());
        if (newID < oldID) {
            for (int i = oldID - 1; i >= newID; i--) {
                int x = i + 1;
                db.execSQL("UPDATE locations SET id = " + x + " WHERE id = " + i + ";");

                Log.d("dane z bazy", "petla  " + i);
            }
        }else if (newID > oldID) {
                for (int i = oldID + 1; i <= newID; i++) {
                    int x = i - 1;
                    db.execSQL("UPDATE locations SET id = " + x + " WHERE id = " + i + ";");
                }
            }
            addSlot(locations, newID);
    }

    private List<Locations>  loadByID(int oldID){
    List<Locations> mList = new ArrayList<>();
    SQLiteDatabase db = getReadableDatabase();
    String[] columns={"id", "description", "firstCoordinate", "secondCoordinate"};
    String args[]={oldID+""};
    Cursor cursor =db.query("locations",columns,"id=?",args,null,null,null,null);
    for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
        Locations locations = new Locations();
        locations.setId(cursor.getInt(0));
        locations.setDescription(cursor.getString(1));
        locations.setFirstCoordinate(cursor.getDouble(2));
        locations.setSecondCoordinate(cursor.getDouble(3));
        mList.add(locations);
    }
    cursor.close();
    return mList;
}


}
