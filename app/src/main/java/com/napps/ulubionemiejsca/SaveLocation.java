package com.napps.ulubionemiejsca;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.content.ContentValues.TAG;

/**
 * Created by Rewan on 2017-08-21.
 */

public class SaveLocation {
    private static final int REQUEST_CODE = 34;
    private View view;
    private Context context;
    private MainActivity mainActivity;
    private FusedLocationProviderClient mFusedLocationClient;
    private BaseManagement bm;
    private String description;
    protected Location mLastLocation;
    private double latitude;
    private double longitude;
    private final OnNewDataAvailable mCallBack;

    interface OnNewDataAvailable {
        void onNewDataAvailable();
    }

    public SaveLocation(OnNewDataAvailable callback, View view, Context context, MainActivity activityCompat) {
        this.context = context;
        this.mCallBack = callback;
        this.view = view;
        this.mainActivity = activityCompat;
        latitude = 0;
        longitude = 0;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

    }

    void makeSave() {
        if (ContextCompat.checkSelfPermission(context, ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            enableGPS();
        } else {
            grantAcces();
        }
    }

    void grantAcces() {
        Snackbar.make(view, R.string.no_premissions, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.grant_acces, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(mainActivity, ACCESS_COARSE_LOCATION)) {
                                    ActivityCompat.requestPermissions(mainActivity, new String[]{ACCESS_COARSE_LOCATION}, REQUEST_CODE);
                                } else {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                                    intent.setData(uri);
                                    context.startActivity(intent);
                                }
                            }
                        }
                ).show();
    }
    void enableGPS() {
        Log.d("Wspolrzedne", "GPS4");
        LocationManager locationManager;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Snackbar.make(view, "Aby pobrać lokalizację...", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Włącz GPS", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent();
                                    intent.setAction(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    context.startActivity(intent);
                                }
                            }
                    ).show();
        } else {

            getLastLocation();

        }
    }
    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {

        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(mainActivity, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            latitude = mLastLocation.getLatitude();
                            longitude = mLastLocation.getLongitude();
                            Log.d("Wspolrzedne", "wspolrzedne to" + latitude + longitude);
                            dialogShow();

                        } else {
                            Log.w(TAG, "getLastLocation:exception", task.getException());
                            Snackbar.make(view, R.string.no_location, Snackbar.LENGTH_INDEFINITE);
                        }
                    }
                });
    }
    private void dialogShow(){
        description = "";
        final Dialog d = new Dialog(context, R.style.Theme_AppCompat_Dialog_Alert);
        d.setTitle(R.string.descriptionTitle);
        d.setContentView(R.layout.dialog);
        Button setButton = (Button) d.findViewById(R.id.setButton);
        Button cancelButton = (Button) d.findViewById(R.id.cancelButton);
        final EditText editDescription = (EditText) d.findViewById(R.id.editDescription);
        setButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                description = editDescription.getText().toString();
                finalizeSave();
                d.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }
    private void finalizeSave(){
            List<Locations> mDataLocations = new ArrayList<>();
            Locations locationObject = new Locations(description, latitude,longitude);
            mDataLocations.add(locationObject);
            BaseManagement baseManagement =new BaseManagement(context);
            baseManagement.addSlot(locationObject);
        if (mCallBack != null) {
            mCallBack.onNewDataAvailable();
        }
    }
    }


